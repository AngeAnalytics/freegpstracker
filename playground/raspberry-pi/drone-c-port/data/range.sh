#! /bin/sh

# Usage: ./range.sh -d FILE

while getopts d: flag
do
    case "${flag}" in
        d) datafile=${OPTARG};;
    esac
done

col1=$(cat $datafile | sed -r '/^\s*$/d' | cut -d' ' -f1 | sort -n)
min1=$(echo "$col1" | tail -1)
max1=$(echo "$col1" | head -1)

col2=$(cat $datafile | sed -r '/^\s*$/d' | cut -d' ' -f2 | sort -n)
min2=$(echo "$col2" | tail -1)
max2=$(echo "$col2" | head -1)

diff1=$(echo "$max1 - $min1" | bc)
diff2=$(echo "$max2 - $min2" | bc)

printf "Pitch range: %s\tRoll range: %s" $diff1 $diff2

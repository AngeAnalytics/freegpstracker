/*
 * A drone using a raspberry pi zero using a PID controller (without the I for now)
 * To debug over wifi, connect to drone via its AP:
 *   nc 192.168.4.1 3000
 *
 * Set up the joystick to send via netcat (extract the channel and value positions, then send):
 *  cat  /dev/input/event20 | stdbuf -o0 xxd -c 24 -ps | stdbuf -o0 cut -c33,34,37,38,41,42 | stdbuf -o0 grep -v "^00" | stdbuf -o0 xxd -r -p | nc 192.168.4.1 3000
 *
 * Motor configuration
 * forward
 *  0(CW)  1(CCW)
 *  3(CCW) 2(CW)
 * back
 */

/* gcc drone.c -o drone -lm -lgsl -lgslcblas -Wall -pthread -lpigpio */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

/* GPIO */
#include <pigpio.h>

/* Server */
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>

#include <string.h>

/* Math */
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

/* I2C + MPU6050 */
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdint.h>

#include <stdbool.h>

#include "dmp.h"

const double Kp = 40; const double Kd = 0.8;
int armed = 0;

/* const int base_pw = 1000; */
/* const int base_pw = 650; */
const int base_pw = 900;

int throttle;

const int pins[] = {18,17,27,22};


/*
     (18)     (gyro)     (17)







     (22)                (27)
 */


int run=1;

int width = 1100;
int pin = 18;

int presocket, newsocket;
/* char buffer[1024]; */
char buffer[296];
struct sockaddr_in server;
struct sockaddr_storage serverutorage;
socklen_t addr_size;

double erroractarr[12] = {1, -1, -1, -1, -1, 1, 1, 1, 1, -1, 1, -1};

double ctrl_values[4] = {0, 0, 0, 0}; // Note: for yaw, joystick control
                                      // indicates relative change
double motorpwrarr[4];

/* const int gyroarr[3] = {-gy.z, -gy.y, gy.x}; */
/* double gyroarr[3]; */

double prevgyroarr[3] = {0, 0, 0};
double prevyprarr[3] = {0, 0, 0};
double yprarr[3];


struct quat {
    double w;
    double x;
    double y;
    double z;
};

struct quat q;           // [w, x, y, z]         quaternion container

struct vecdouble {
  double x;
  double y;
  double z;
};

struct vecdouble grav;

long map(long x, long in_min, long in_max, long out_min, long out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void serverinit() {
  presocket = socket(PF_INET, SOCK_STREAM, 0);
  /* Set socket state to nonblocking, so drone doesnt freeze when no
   instructions are received. */
  fcntl(presocket, F_SETFL, O_NONBLOCK);

  server.sin_family = AF_INET;
  server.sin_port = htons(7891);
  server.sin_addr.s_addr = INADDR_ANY; // tcp socket @ localhost
  /* Zeropad address field */
  bzero(&(server.sin_zero), 8);

  if (0 == bind(presocket, (struct sockaddr *) &server, sizeof(server)))
    printf("Bound socket\n");
  else
    printf("Error: Socket bind failure\n");

  if (listen(presocket, 5) == 0)
    printf("Listening\n");
  else
    printf("Error\n");

  printf("Conn\n");
}

/*
  MPU6050 i2c reading adapted from:
  https://openest.io/en/services/mpu6050-accelerometer-on-raspberry-pi

  and

  https://github.com/richardghirst/PiBits/blob/master/MPU6050-Pi-Demo/I2Cdev.cpp
*/



int file = -1;


// Please note, this is not the recommanded way to write data
// to i2c devices from user space.
void i2c_write(__u8 reg_address, __u8 val) {
    char buf[2];
    if(file < 0) {
        printf("Error, i2c bus is not available\n");
        exit(1);
    }

    buf[0] = reg_address;
    buf[1] = val;

    if (write(file, buf, 2) != 2) {
        printf("Error, unable to write to i2c device\n");
        exit(1);
    }

}

// Please note, this is not thre recommanded way to read data
// from i2c devices from user space.
char i2c_read(uint8_t reg_address) {
    char buf[1];
    if(file < 0) {
        printf("Error, i2c bus is not available\n");
        exit(1);
    }

    buf[0] = reg_address;

    if (write(file, buf, 1) != 1) {
        printf("Error, unable to write to i2c device\n");
        exit(1);
    }


    if (read(file, buf, 1) != 1) {
        printf("Error, unable to read from i2c device\n");
        exit(1);
    }

    return buf[0];

}

int8_t i2c_readbit(uint8_t regAddr, uint8_t bitNum) {
    uint8_t b;
    b = i2c_read(regAddr);
    return b & (1 << bitNum);
}

void i2c_writebit(uint8_t regAddr, uint8_t bitNum, uint8_t data) {
    uint8_t b;
    b = i2c_read(regAddr);
    b = (data != 0) ? (b | (1 << bitNum)) : (b & ~(1 << bitNum));
    i2c_write(regAddr, b);
}

bool i2c_writebytes(uint8_t regAddr, uint8_t length, uint8_t* data) {
    int8_t count = 0;
    uint8_t buf[128];

    if (length > 127) {
        fprintf(stderr, "Byte write count (%d) > 127\n", length);
        return false;
    }

    buf[0] = regAddr;
    memcpy(buf+1,data,length);
    count = write(file, buf, length+1);
    if (count < 0) {
        fprintf(stderr, "Failed to write device(%d):\n", count);
        close(file);
        return false;
    } else if (count != length+1) {
        fprintf(stderr, "Short write to device, expected %d, got %d\n", length+1, count);
        close(file);
        return false;
    }

    return true;
}

int8_t i2c_readbytes(uint8_t regAddr, uint8_t length, uint8_t *data) {
    int8_t count = 0;

    if (write(file, &regAddr, 1) != 1) {
        fprintf(stderr, "Failed to write reg: %s\n", strerror(errno));
        close(file);
        return(-1);
    }
    count = read(file, data, length);
    if (count < 0) {
        fprintf(stderr, "Failed to read device(%d):\n", count);
        close(file);
        return(-1);
    } else if (count != length) {
        fprintf(stderr, "Short read to device, expected %d, got %d\n", length+1, count);
        close(file);
        return(-1);
    }

    return count;
}


/*
  MPU6050 ADDRESSES

  See

  https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-6000-Datasheet1.pdf

  For usage
*/
#define REG_ACCEL_ZOUT_H              0x3F
#define REG_ACCEL_ZOUT_L              0x40
#define REG_PWR_MGMT_1                0x6B
#define REG_ACCEL_CONFIG              0x1C
#define REG_SMPRT_DIV                 0x19
#define REG_CONFIG                    0x1A
#define REG_FIFO_EN                   0x23
#define REG_USER_CTRL                 0x6A
#define REG_FIFO_COUNT_L              0x72
#define REG_FIFO_COUNT_H              0x73
#define REG_FIFO                      0x74
#define REG_WHO_AM_I                  0x75
#define REG_INT_ENAB                  0x38
#define REG_INT_STAT                  0x3A

#define MPU6050_I2C_ADDR              0x68
#define MPU6050_RA_BANK_SEL           0x6D
#define MPU6050_RA_MEM_START_ADDR     0x6E
#define MPU6050_RA_MEM_R_W            0x6F

#define MPU6050_DMP_MEMORY_CHUNK_SIZE 16

/* #define DMP_PACKET_SIZE               42 */
#define DMP_PACKET_SIZE               12

#define pgm_read_byte(p) (*(uint8_t *)(p))

#define PI 3.14159265

void MPUSetMemoryBank(uint8_t bank, bool prefetchEnabled, bool userBank) {
  bank &= 0x1F;
  if (userBank) bank |= 0x20;
  if (prefetchEnabled) bank |= 0x40;
  i2c_write(MPU6050_RA_BANK_SEL, bank);
}

void MPUSetMemoryStartAddress(uint8_t address) {
  i2c_write(MPU6050_RA_MEM_START_ADDR, address);
}

bool writeMemoryBlock(const uint8_t *data, uint16_t dataSize, uint8_t bank, uint8_t address, bool verify) {
  uint8_t chunkSize, *verifyBuffer;
  uint8_t *progBuffer = NULL; // Keep compiler quiet
  uint16_t i; uint8_t j;

  MPUSetMemoryBank(bank, false, false);
  MPUSetMemoryStartAddress(address);

  bool useProgMem = true;

  if (verify) verifyBuffer = (uint8_t *)malloc(MPU6050_DMP_MEMORY_CHUNK_SIZE);
  if (useProgMem) progBuffer = (uint8_t *)malloc(MPU6050_DMP_MEMORY_CHUNK_SIZE);
  for (i = 0; i < dataSize;) {
    // determine correct chunk size according to bank position and data size
    chunkSize = MPU6050_DMP_MEMORY_CHUNK_SIZE;

    // make sure we don't go past the data size
    if (i + chunkSize > dataSize) chunkSize = dataSize - i;

    // make sure this chunk doesn't go past the bank boundary (256 bytes)
    if (chunkSize > 256 - address) chunkSize = 256 - address;

    if (useProgMem) {
      // write the chunk of data as specified
      for (j = 0; j < chunkSize; j++) progBuffer[j] = pgm_read_byte(data + i + j);
    } else {
      // write the chunk of data as specified
      progBuffer = (uint8_t *)data + i;
    }

    i2c_writebytes(MPU6050_RA_MEM_R_W, chunkSize, progBuffer);

    // verify data if needed
    if (verify && verifyBuffer) {
      MPUSetMemoryBank(bank, false, false);
      MPUSetMemoryStartAddress(address);
      i2c_readbytes(MPU6050_RA_MEM_R_W, chunkSize, verifyBuffer);
      if (memcmp(progBuffer, verifyBuffer, chunkSize) != 0) {
        free(verifyBuffer);
        if (useProgMem) free(progBuffer);
        return false; // uh oh.
      }
    }

    // increase byte index by [chunkSize]
    i += chunkSize;

    // uint8_t automatically wraps to 0 at 256
    address += chunkSize;

    // if we aren't done, update bank (if necessary) and address
    if (i < dataSize) {
      if (address == 0) bank++;
      MPUSetMemoryBank(bank, false, false);
      MPUSetMemoryStartAddress(address);
    }
  }
  if (verify) free(verifyBuffer);
  if (useProgMem) free(progBuffer);
  return true;
}

bool writeDMPConfigurationSet(const uint8_t *data, uint16_t dataSize) {
    uint8_t *progBuffer = NULL, success, special;
    uint16_t i, j;

    bool useProgMem = false;

    if (useProgMem) {
        progBuffer = (uint8_t *)malloc(8); // assume 8-byte blocks, realloc later if necessary
    }

    // config set data is a long string of blocks with the following structure:
    // [bank] [offset] [length] [byte[0], byte[1], ..., byte[length]]
    uint8_t bank, offset, length;
    for (i = 0; i < dataSize;) {
        if (useProgMem) {
            bank = pgm_read_byte(data + i++);
            offset = pgm_read_byte(data + i++);
            length = pgm_read_byte(data + i++);
        } else {
            bank = data[i++];
            offset = data[i++];
            length = data[i++];
        }

        // write data or perform special action
        if (length > 0) {
            // regular block of data to write
            /*Serial.print("Writing config block to bank ");
            Serial.print(bank);
            Serial.print(", offset ");
            Serial.print(offset);
            Serial.print(", length=");
            Serial.println(length);*/
            if (useProgMem) {
                if (sizeof(progBuffer) < length) progBuffer = (uint8_t *)realloc(progBuffer, length);
                for (j = 0; j < length; j++) progBuffer[j] = pgm_read_byte(data + i + j);
            } else {
                progBuffer = (uint8_t *)data + i;
            }
            success = writeMemoryBlock(progBuffer, length, bank, offset, true);
            i += length;
        } else {
            // special instruction
            // NOTE: this kind of behavior (what and when to do certain things)
            // is totally undocumented. This code is in here based on observed
            // behavior only, and exactly why (or even whether) it has to be here
            // is anybody's guess for now.
            if (useProgMem) {
                special = pgm_read_byte(data + i++);
            } else {
                special = data[i++];
            }
            /*Serial.print("Special command code ");
            Serial.print(special, HEX);
            Serial.println(" found...");*/
            if (special == 0x01) {
                // enable DMP-related interrupts

                //setIntZeroMotionEnabled(true);
                //setIntFIFOBufferOverflowEnabled(true);
                //setIntDMPEnabled(true);
                i2c_write(REG_INT_ENAB, 0x32);  // single operation

                success = true;
            } else {
                // unknown special command
                success = false;
            }
        }

        if (!success) {
            if (useProgMem) free(progBuffer);
            return false; // uh oh
        }
    }
    if (useProgMem) free(progBuffer);
    return true;
}

void resetfifo() {
  i2c_writebit(REG_USER_CTRL, 2, true);
}

void setfifoenabled(bool enabled) {
  i2c_writebit(REG_USER_CTRL, 6, enabled);
}

void setdmpenabled(bool enabled) {
  i2c_writebit(REG_USER_CTRL, 7, enabled); // enable dmp
}

void resetdmp() {
  i2c_writebit(REG_USER_CTRL, 3, true); // enable dmp
}

#define MPU6050_RA_DMP_CFG_1        0x70

void setdmpconf1(uint8_t config) {
    i2c_write(MPU6050_RA_DMP_CFG_1, config);
}

#define MPU6050_RA_DMP_CFG_2        0x71

void setdmpconf2(uint8_t config) {
    i2c_write(MPU6050_RA_DMP_CFG_2, config);
}

uint16_t merge_bytes( uint8_t LSB, uint8_t MSB) {
  return  (uint16_t) ((( LSB & 0xFF) << 8) | MSB);
}

// 16 bits data on the MPU6050 are in two registers,
// encoded in two complement. So we convert those to int16_t
int16_t two_complement_to_int( uint8_t LSB, uint8_t MSB) {
  int16_t signed_int = 0;
  uint16_t word;

  word = merge_bytes(LSB, MSB);

  if((word & 0x8000) == 0x8000) { // negative number
        signed_int = (int16_t) -(~word);
    } else {
        signed_int = (int16_t) (word & 0x7fff);
    }

    return signed_int;
}


char accel_x_h,accel_x_l;
uint16_t fifo_len = 0;
float x_gyro_g, y_gyro_g, z_gyro_g;

  /*
    Data is written to the FIFO in order of register number (from
    lowest to highest). If all the FIFO enable flags (see below) are
    enabled and all External Sensor Data registers (Registers 73 to
    96) are associated with a Slave device, the contents of registers
    59 through 96 will be written in order at the Sample Rate.
  */

uint8_t dmpdecodequat(int16_t *data, const uint8_t* packet) {
  // if (packet == 0) packet = dmpPacketBuffer;
  data[0] = ((packet[0] << 8) + packet[1]);
  data[1] = ((packet[4] << 8) + packet[5]);
  data[2] = ((packet[8] << 8) + packet[9]);
  data[3] = ((packet[12] << 8) + packet[13]);
  return 0;
}

uint8_t dmpgetquat(struct quat *q, const uint8_t* packet) {
  int16_t qI[4];
  uint8_t status = dmpdecodequat(qI, packet);
  if (status == 0) {
    q -> w = (double)qI[0] / 16384.0f;
    q -> x = (double)qI[1] / 16384.0f;
    q -> y = (double)qI[2] / 16384.0f;
    q -> z = (double)qI[3] / 16384.0f;
    return 0;
  }
  return status; // int16 return value, indicates error if this line is reached
}

uint8_t calcypr(double *data, struct quat *q, struct vecdouble *gravity) {
    // yaw: (about Z axis)
    data[0] = atan2(2*q->x*q->y - 2*q->w*q->z, 2*q->w*q->w + 2*q->x*q->x - 1);
    // pitch: (nose up/down, about Y axis)
    data[1] = atan(gravity->x / sqrt(gravity->y*gravity->y + gravity->z*gravity->z));
    // roll: (tilt left/right, about X axis)
    data[2] = atan(gravity->y / sqrt(gravity->x*gravity->x + gravity->z*gravity->z));
    return 0;
}

uint8_t calcgrav(struct vecdouble *v, struct quat *q) {
    v->x = 2 * (q->x*q->z - q->w*q->y);
    v->y = 2 * (q->w*q->x + q->y*q->z);
    v->z = q->w*q->w - q->x*q->x - q->y*q->y + q->z*q->z;
    return 0;
}

void stop(int signum)
{
   run = 0;
}

int main(int argc, char *argv[]) {

  /* INITIALIZE SERVER */

  serverinit();

  /* INITIALIZE MOTORS */
  if (gpioInitialise() < 0)
    return -1;

  gpioSetSignalFunc(SIGINT, stop);

  printf("\nMOTORINIT\n");

  for (int i = 0; i < 4; ++i) {
    printf("M%d, %d\n", pins[i], 1000);
    gpioServo(pins[i], 1000);
  }

  sleep(2);

  /* /\* Test Motors *\/ */
  /* printf("\nMOTORTEST\n"); */

  /* for (int i = 0; i < 4; ++i) { */
  /*   printf("M%d, %d\n", pins[i], 1000); */
  /*   gpioServo(pins[i], 1200); */
  /* } */

  /* sleep(1); */

  printf("\nBASEPW\n");

  for (int i = 0; i < 4; ++i) {
    printf("M%d, %d (base_pw)\n", pins[i], base_pw);
    gpioServo(pins[i], base_pw);
  }

  sleep(2);

  int adapter_nr = 1; // Device ID
  char bus_filename[250];

  snprintf(bus_filename, 250, "/dev/i2c-%d", adapter_nr);
  file = open(bus_filename, O_RDWR);
  if (file < 0) {
    fprintf(stderr, "Failed to open device: %s\n", strerror(errno));
    exit(1);
  }

  if (ioctl(file, I2C_SLAVE, MPU6050_I2C_ADDR) < 0) {
    fprintf(stderr, "Failed to select device: %s\n", strerror(errno));
    close(file);
    exit(1);
  }

  /* MPU6040 SETUP */
  i2c_write(REG_PWR_MGMT_1, 0x01);
  i2c_write(REG_ACCEL_CONFIG, 0x00);
  i2c_write(REG_SMPRT_DIV, 0x07);
  /* i2c_write(REG_CONFIG, 0x01); // Enable digital low pass filter */
  i2c_write(REG_CONFIG, 0x11); // Enable digital low pass filter
  i2c_write(REG_FIFO_EN, 0x78); // Reg0x23 11110000
                                // We need gravity and gyro readings
  i2c_write(REG_USER_CTRL, 0x44);
  i2c_write(REG_INT_ENAB, 0x01); // Reg0x38 00000001
                                 // Enable interrupt status


  /* MPU NEW */
  /* mpu.initialize(); unnecessary */
  /* mpu.dmpInitialize(); done */
  /* mpu.CalibrateAccel(6); */
  /* mpu.CalibrateGyro(6); */
  /* mpu.setDMPEnabled(true); done */

  /* dmpInitialize begin */

  /* i2c_writebit(REG_PWR_MGMT_1, 7, true); // reset dmp */

  /* Selecting user bank 16... */
  MPUSetMemoryBank(0x10, true, true);
  /* Selecting memory byte 6... */
  MPUSetMemoryStartAddress(0x06);
  /* Resetting memory bank selection to 0... */
  MPUSetMemoryBank(0, false, false);

  if (writeMemoryBlock(dmpMemory, MPU6050_DMP_CODE_SIZE, 0, 0, true)) {
    printf("Success! DMP code written and verified.\n");
  } else {
    printf("FAILURE\n");
  }

  if (writeDMPConfigurationSet(dmpConfig, MPU6050_DMP_CONFIG_SIZE)) {
    printf("Success! DMP configuration written and verified.\n");
  } else {
    printf("FAILURE\n");
  }

  setdmpconf1(0x03);
  setdmpconf2(0x00);

  resetfifo();
  setfifoenabled(true);
  setdmpenabled(true);
  resetdmp(); // break

  /* dmpInitialize done */

  setdmpenabled(true);

  double gyroarr[] = {-z_gyro_g, -y_gyro_g, x_gyro_g};
  double targetarr[] = {ctrl_values[0], ctrl_values[1], ctrl_values[2]};

  /* Initialize GSL vector views */
  gsl_vector_view target = gsl_vector_view_array(targetarr, 3);
  gsl_vector_view gyro = gsl_vector_view_array(gyroarr, 3);
  gsl_vector_view prevgyro = gsl_vector_view_array(prevgyroarr, 3);
  gsl_vector_view ypr = gsl_vector_view_array(yprarr, 3);
  gsl_vector_view prevypr = gsl_vector_view_array(prevyprarr, 3);

  gsl_matrix_view erroract = gsl_matrix_view_array(erroractarr, 4, 3);
  gsl_matrix_view gyromat = gsl_matrix_view_array(gyroarr, 3, 1);
  gsl_matrix_view motorpowers = gsl_matrix_view_array(motorpwrarr, 4, 1);

  /* RUN */
  while (run) {
    if (armed != 1) {
      if (ctrl_values[0] < -100 && ctrl_values[1] < -100 &&
          ctrl_values[2] < -100 && ctrl_values[3] < -100) {
        armed = 1;
      }
    }

    armed = 1;

    /* Accept incoming connections */
    int s;
    if (!newsocket || newsocket == -1 || s == 0) {
      addr_size = sizeof serverutorage;
      newsocket = accept(presocket, (struct sockaddr *)&serverutorage, &addr_size);
      fcntl(newsocket, F_SETFL, O_NONBLOCK);
    }

    /* Read incoming instruction */
    s = recv(newsocket, buffer, 296, 0);

    if (!(s == 0 || s < 0)) {
      printf("Received: %s\n\n", buffer);
      memset(buffer, 0, sizeof(buffer));
    }

    /* uint8_t tcp_rx_buffer[296]; */
    /* const int nbBytesRead = client.read(tcpRxBuffer, 296); */
    /* for (int i = 0; i < nbBytesRead; i += 3) { */
    /*   ctrl_values[tcpRxBuffer[i + 1]] = tcpRxBuffer[i + 2]; */
    /* } */

    int ready = i2c_read(REG_INT_STAT);
    /* printf("%d ", ready & 1); */

    if(!((ready & 1) == 1)){
      printf("NOT READY\n");
      /* sleep(1); */
      continue;
    }


    uint8_t fifobuf[64]; // FIFO storage buffer

    /* get fifo buffer length */
    accel_x_h = i2c_read(REG_FIFO_COUNT_L);
    accel_x_l = i2c_read(REG_FIFO_COUNT_H);
    fifo_len = merge_bytes(accel_x_h, accel_x_l);

    /* printf("FIFO LEN:%d\n", fifo_len); */

    if (fifo_len == 1024) {
      i2c_write(REG_USER_CTRL, 0x44); // reset fifo
      printf("FIFO OVERFLOW\n");
      continue;
    }

    if (fifo_len >= 12) {

      /* printf("Reading MPU\n\n"); */

      i2c_readbytes(REG_FIFO, DMP_PACKET_SIZE, fifobuf); // read fifo buffer

      dmpgetquat(&q, fifobuf);

      calcgrav(&grav, &q);
      calcypr(yprarr, &q, &grav);



      printf("accel: x %7.3fg   y %7.3fg   z %7.3fg\t",
             q.x, q.y, q.z);
      printf("gyro: x %7.3f°/s   y %7.3f°/s   z %7.3f°/s\t",
             grav.x, grav.y, grav.z);
      /* printf("ypr: %.3f %.3f %.3f", yprarr[0], yprarr[1], yprarr[2]); */
      printf("ypr  %7.2f %7.2f %7.2f", yprarr[0]*180/PI, yprarr[1]*180/PI, yprarr[2]*180/PI);

      /* gsl_blas_daxpy(-1, &target.vector, &gyro.vector); */

      gsl_vector_scale(&ypr.vector, 0.2);
      gsl_vector_scale(&prevypr.vector, 0.8);
      gsl_vector_add(&ypr.vector, &prevypr.vector);

      /*
       gyro = 0.6 * gyro + 0.4 * prevgyro
       low-pass filter the derivative
      */
      gsl_vector_scale(&gyro.vector, 0.4);
      gsl_vector_scale(&prevgyro.vector, 0.6);
      gsl_vector_add(&gyro.vector, &prevgyro.vector);

      int i;

      for (i = 0; i < 3; i++) {
        prevgyroarr[i] = gyroarr[i];
        prevyprarr[i] = yprarr[i];
      }

      /*
       motorpowers = errortoaction * (Kp * yprMat + Kd * gyro - target)
      */
      gsl_vector_scale(&gyro.vector, Kd);
      gsl_vector_scale(&ypr.vector, Kp);
      gsl_vector_add(&gyro.vector, &ypr.vector);
      gsl_vector_sub(&gyro.vector, &target.vector);

      gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, &erroract.matrix,
                     &gyromat.matrix, 0.0, &motorpowers.matrix);

      throttle = map(ctrl_values[3], -128, 127, 0, 1000) + base_pw;

      if (armed == 1) {
        for (int i = 0; i < 4; ++i) {
          gpioServo(pins[i], motorpwrarr[i] + throttle);
          printf(" %f", motorpwrarr[i] + throttle);
        }

      }
      prevgyro = gyro;

      printf("\n");

      usleep(10000);

    } else {
      usleep(10000);
    }
  }

  /* SHUTDOWN */
  printf("\n\nTidying up\n");

  for (int i = 0; i < 4; ++i) {
    gpioServo(pins[i], 1000); // 0 or 1000?
    printf("M%d, %d\n", pins[i], 1000);
  }

  gpioTerminate();

  close(file);

  return 0;
}

// Plant watering system. 
// The system has 3 leds: Green, Yellow and Red
// A touch sensor composed of 2 paralell wires detects if the user touches the 2 wires. If the user touches the wires briefly, the status is reported:
// - Green/Red alternating: The water is optimal
// - 1 to 3 green flashes: Too much water
// - 1 to 3 ref flashes: not enough water
// If you press more than 2 seconds, the yellow led flashes and the currect moisture is read and used as reference.
// You need to water the plant, depending on the plant, wait until the moisture is correct for the plant. 
// Once achieved, press for 2 seconds and that moisture will be used as reference.
// WHen the moisture falls below optimal, the system will beep every 10 minutes or so
// The system enters sleeps but wakes up every second to check for a touch
// A better solution would be to make the system sleep for max time and put a real switch that caused an interupt to wake up the arduino
//---------------load libraries
#include "LowPower.h"
//---------------define digital pins
#define greenPin 10
#define redPin 9
#define yellowPin 8
#define beepPin 5
#define sensorPin A1
#define touchPower 12
#define touchPin A0
#define probePin 11
//---------------definition of limit
int MaxRange = 610;
//---------------variables
int LastBeepSeconds = 0; //only beep every so many intervals
int CurrentValue = 0;
int LastValue = 0;
//---------------read probe
int ReadProbe() {
    //Read the probe
    digitalWrite(probePin,HIGH);
    delay(10);
    int valueSensor = analogRead(sensorPin);
    digitalWrite(probePin,LOW);
    return valueSensor;
}
//--------------led blinks
void Blink(String colour, int count, String len) {
  int pin;
  if (colour == "G") pin = greenPin;
  if (colour == "Y") pin = yellowPin;
  if (colour == "R") pin = redPin;
  for (int i = 0; i < count; i++) {
    digitalWrite(pin,HIGH);
    delay(len == "short"?120:500);
    digitalWrite(pin,LOW); 
    delay(120);   
  }
}
//---------------beeps
void Beep() {
  if(LastBeepSeconds > 600 || LastBeepSeconds == 0){
    tone(beepPin,7000,40);
    delay(40);
    LastBeepSeconds = 0;
  }
}


//----------------DisplayValue
void   DisplayValue(int value) {
  int blinkcount = map(value, MaxRange-60, MaxRange, -5, 5);
  Serial.println("blinkcount: " + (String) blinkcount);
  Serial.println("value: " + (String) value);  
  if (blinkcount == 0) {
    Blink("G",1,"short");
    Blink("R",1,"short");
    Blink("G",1,"short");
    Blink("R",1,"short");
}
  bool overflow = abs(blinkcount) > 5;
  if (blinkcount > 0) {
    Blink("G",blinkcount>5?5:blinkcount,"short");
    if (overflow) Blink("G",1,"long");
  }
  if (blinkcount < 0) {
    Blink("R",blinkcount<-5?5:-blinkcount,"short");
        if (overflow) Blink("R",1,"long");
  }
}
//----------------GoToSleep
void GoToSleep(int seconds) {
  //sleeps for seconds but wakeup every second to see if the touch sensor is activated.
  for (int i = 0; i < seconds; i++) {
    //delay(100);
    LastBeepSeconds++; // add 1 second
    LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF); 
    
    if(GetTouchValue()>200) break;
  }
}
int GetTouchValue() {
    digitalWrite(touchPower,HIGH);   
    delay(10);
    int touchvalue = analogRead(touchPin);
    delay(10);
    digitalWrite(touchPower,LOW);  
    return touchvalue;
}
//----------------CheckTouchMode
void CheckTouchMode() {
    int touchvalue = GetTouchValue();
    Serial.println("Touch value is " + (String) touchvalue);
    if(touchvalue < 200 ) return;
    LastBeepSeconds = 0;
    //Loop and if the button is high for more than 2 seconds, enter setup mode
    for (int i = 0; i<6; i++) {
      Blink("Y",1,"short"); //blink Once
      delay(100);
      touchvalue = GetTouchValue();
      Serial.println("....Touch value is " + (String) touchvalue);
      if (touchvalue < 200) break;
    }
    if (touchvalue < 200) {
      DisplayValue(CurrentValue);
    } else {
      // setup is entered.
      digitalWrite(yellowPin,HIGH);
      delay(1000);
      digitalWrite(yellowPin,LOW);
      MaxRange = ReadProbe()+15;
    }
}
//-----------------Setup
void setup()
 {
    Serial.begin(9600); 
    pinMode(13,OUTPUT); //arduino led
    pinMode(beepPin,OUTPUT); //beep
    pinMode(redPin,OUTPUT); //Red
    digitalWrite(redPin,LOW);
    pinMode(greenPin,OUTPUT); //Green
    digitalWrite(greenPin,LOW);
    pinMode(yellowPin,OUTPUT); //Yellow
    digitalWrite(yellowPin,LOW);
    pinMode(probePin,OUTPUT); //Yellow
    digitalWrite(probePin,LOW);
    pinMode(touchPower,OUTPUT); //Yellow
    digitalWrite(touchPower,LOW);
    pinMode(touchPin,INPUT);
    Serial.println("Starting...");
    //POSt    
    tone(beepPin,5000,60);
    delay(60);
    Blink("G",1,"short");
    Blink("Y",1,"short");
    Blink("R",1,"short");
    //delay(1000);
    CurrentValue = ReadProbe();   
    Serial.println("Initial Probe: " + (String) CurrentValue);
    Beep();
    LastBeepSeconds = 0;
 }
//--------------------Loop
void loop(){ 
    LastValue = CurrentValue;
    CurrentValue = ReadProbe();
    Serial.println("value: " + (String) CurrentValue);
    CheckTouchMode();
    if (CurrentValue < MaxRange-30 && CurrentValue > 100) { // 
      DisplayValue(CurrentValue);
      Beep();
    }
    if (LastValue <= 100 and CurrentValue > 100) DisplayValue(CurrentValue);
    if (CurrentValue <= 100) Beep();
    // sleep for a minute
    GoToSleep(60);
    //delay(10000);
}
 

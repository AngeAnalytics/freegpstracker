/**
 * Plain do-nothing code but create a GSM instance and call gsmAccess.begin() followed by a call to gsmAccess.shutdown()

 * On reset current raises to ~40-160 mA and stabilizes after 10-20 seconds at varying levels, for instance
 *  * 20.5 mA +/- 0.1 (4 consecutive samples)
 *  * 21.3 mA +/- 0.1 (1 consecutive samples)
 *  * 20.5 mA +/- 0.1 (2 consecutive samples)
 *  * 21.4 mA +/- 0.1 (1 consecutive samples)
 *  * 20.8 mA +/- 0.1 (1 consecutive samples)
 *  * 20.5 mA +/- 0.1 (1 consecutive samples)
 */

#include <MKRGSM.h>

GSM gsmAccess;

void setup() {
    gsmAccess.begin();
    gsmAccess.shutdown();
}

void loop() {
}

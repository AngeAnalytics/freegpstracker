/**
 * Plain do-nothing code but create a GSM instance and call gsmAccess.begin()
 * On reset current raises to ~40-160 mA and stabilizes after 10-20 seconds at varying levels, for instance
 *  * 20.2 mA +/- 0.1 (3 consecutive samples)
 *  * 17.0 mA +/- 0.1 (1 sample)
 *  * 26.0 +/- 1.0 with spikes at 33mA (1 sample)
 *  * 19.7 +/- 0.1 (5 consecutive samples)
 */

#include <MKRGSM.h>

GSM gsmAccess;

void setup() {
    gsmAccess.begin();
}

void loop() {
}

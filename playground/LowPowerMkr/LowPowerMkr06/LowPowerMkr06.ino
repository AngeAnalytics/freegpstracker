/**
 * Plain do-nothing code with a call to LowPower.deepSleep(10000);
 *
 * On reset current raises to ~34 mA and stabilizes after a couple of seconds at varying levels, 
 *  * 15.1 mA +/- 0.1 Then raises to 25.3 +/- 0.1 (at ~12 secs) (10 samples)
 
 */

#include <ArduinoLowPower.h>
void setup() {
    LowPower.deepSleep(10000);
}

void loop() {
}

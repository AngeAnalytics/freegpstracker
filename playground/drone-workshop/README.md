The drone workshop - Instructor's notes
=======================================

A set of educational modules eventually leading to building (programming) a fully functional drone:
 * a test for the IMU (Inertial Measurement Unit)
 * a test and calibration program for the ESCs (Electronic Speed Controllers) and motors
 * a test for the radio controller (WiFi based)
 * the main program linking it all together

The workshop can be done in 2-8 hours, depending on the level of detail at which the modules are investigated and whether it's done in parallel by multiple teams.

The workshop is mainly targeted at students aged 12+ but has been accomplished by younger children. It also addresses advanced engineering notions such as transfer functions and Laplace transforms that are relevant at any age.

Prerequisites
-------------
The following hardware is required:
 * a microcontroller (e.g. an Arduino Uno)
 * an IMU (e.g. a MPU6050)
 * a radio control system (e.g. a ESP8266 WiFi module, a Taranis x9d + a WiFi-capable laptop)
 * a drone chassis + ESCs + motors + battery (e.g. a F450 frame + 30A Simonk ESCs + 1000kV motors. Note: drones heavier than 250g require a permit to fly in a public space)
 * screws and mount to fix the microcontroller to the chassis
 * soldering iron if the drone needs to be built from scratch
 * multimeter and preferably also an oscilloscope

The teacher needs a basic understanding of programming and electronics and/or to have completed the workshop previously.


Content/objectives
------------------
The workshop allows the students to:
 * use programming as a means to build something very concrete and tangible
 * get an understanding of electronics, both from a signal perspective and a power perspective
 * understand how information can be transmitted electrically (serial bus, i2c bus and PWM)
 * get hands-on experience with the notions of inputs, outputs and the abstract concept of a *function*
 * get a feeling of how modifying the control parameters changes the stability profile of a system
 * build a platform that can subsequently be used for all kinds of other projects


Pedagogical pointers
--------------------

 * create an early experience of success. If needed, use the introduction of [Khan Academy](https://www.khanacademy.org/computer-programming/new/pjs) to get a rapid code-result cycle and the ensuing Aha-experience. The Khan Academy approach (setup, draw) is based on the Processing pattern just like Arduino (setup, loop).
 * interleave hands-on activities (soldering, measurements, flying) with the perhaps more tedious typing and coding
 * try and make the students discover as much as they can by themselves. The suggested approach is to print a stub of each of the test programs on a piece of paper and have the students finish the programs in the Arduino IDE. Depending on how autonomous the students are, the stub can be more or less complete.
 * present the microcontroller as a *function*: it has an input, an output *and that's all*. Getting a good grasp of the notion of function at an early age is an invaluable win.

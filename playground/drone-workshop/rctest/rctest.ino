/*
 * A test for the radio controller (over WiFi)
 */

#include "radiocontroller.h"

RadioController rc("efdrobot");

void setup() {
    Serial.begin(115200);
    rc.init();
}

void loop() {
    const int nbBytesRead = rc.read();
    if (nbBytesRead > 0) {
        Serial.print("Bytes read: ");
        Serial.print(nbBytesRead);
        Serial.print("    ");
        for (int i = 0; i < 4; ++i) {
            Serial.print(rc.values[i]);
            Serial.print("  ");
        }
        Serial.println();
    }
}

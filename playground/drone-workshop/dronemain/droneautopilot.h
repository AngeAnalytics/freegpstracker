class DroneAutopilot {
public:
    void init() {
        timer = millis();

      }
    bool isActive() {return false;}
    Matrix<3> targetAttitude(){return {0.0, 0.0, 0.0};};
    float throttle() {
      if (millis() - timer < 1000){
        return 0;
      } else if (millis() - timer <  2000){
        return 300;
      } else  if(millis()- timer  < 2500){
        return 500
        ;
      } else {
        return 0;
      }
    }
private:
    long int timer = 0;
};

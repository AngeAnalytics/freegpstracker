/*
 * A drone using an Arduino Uno
 */

#include "gyro.h"
#include "pidcontroller.h"
#include "radiocontroller.h"
#include "motors.h"
#include "droneautopilot.h"

const float Kp = 60, Kd = 1.2;
bool isArmed = false;
Motors motors((const int[]){3,9,10,11});
Gyro gyro;
RadioController rc("efdrobot");
DroneAutopilot autopilot;

void setup() {
    motors.init();
    gyro.init();
    rc.init();
}

void loop() {
    rc.read();
    if (!isArmed) {

        if (rc.values[0] < -100 && rc.values[1] < -100 && rc.values[2] < -100 && rc.values[3] < -100) {
            isArmed = true;
            autopilot.init();
        }
    }
    if (isArmed && gyro.isReady()) {
        GyroReadings gyroReadings = gyro.read();
        Matrix <3> target = {0.0, 0.0, 0.0};
        int throttle = 0;
        if (autopilot.isActive()) {
            target = autopilot.targetAttitude();
            throttle = autopilot.throttle();
        } else {
            target = {rc.values[0] , rc.values[1], rc.values[2]}; //Note: for yaw, joystick control indicates relative change
            throttle = map(rc.values[3], -128, 127, 0, 1000);
        }
        PidController pidController({Kp, Kd, gyroReadings.yprMat, gyroReadings.gyro, target});
        if (isArmed) {
            motors.activate(pidController.motorPowers(), throttle);
        }
    }
}

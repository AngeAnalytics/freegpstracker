#include "HttpStreamClient.h"
#include "wificonfig.h"
#include "olimexcontroller.h"
#include "distance-scanner.h"
#ifndef CAMERA_DISABLED
#include "ArduCamStream.h"
#endif
#include "StringStream.h"

#ifdef ARDUINO_SAMD_MKRGSM1400
    #include <MKRGSM.h>
    GSMClient tcpClient;
    GPRS gprs;
    GSM gsmAccess;
#elif ARDUINO_SAMD_MKRNB1500
    #include <MKRGSM.h>
    NBClient tcpClient;
    GPRS gprs;
    NB nbAccess;
#elif defined(ARDUINO_AVR_UNO_WIFI_REV2) || defined(ARDUINO_SAMD_NANO_33_IOT)
    #include <WiFiNINA.h>
    WiFiClient tcpClient;
#else
    #include "WiFiEspAT.h"
    WiFiClient tcpClient;
#endif

HttpStreamClient httpClient(&tcpClient);
OlimexController motorController(4, 5, 6, 7);
bool connectionOK = false;
bool cameraOK = false;
int servoPin = A3;
Servo myservo;
const int inputPin = 8;
const int outputPin = 9;

#ifndef CAMERA_DISABLED
ArduCamStream cameraStream;

#ifdef ARDUINO_ARCH_SAMD
const int CAM_CS_PIN = 7;
#else
const int CAM_CS_PIN = 10;
#endif
#endif

long int statusTimer;
char command[32];
unsigned long commandTimer;
unsigned long commandPeriod = 1000;
int connectionAttempts = 0;

const char* reportMeasurement(const char* measurementContent) {
    char measurementRequest[70];
    sprintf_P(measurementRequest, PSTR("/write/measurement/%s/%s"), robotId, measurementContent);
    return httpClient.get(commandServer, measurementRequest, commandServerPort);
}

const char* reportDiagnostic() {
    StringStream diagnosticStream;
    char diagnostic[100];
    int uptime = millis() / 1000;
    sprintf_P(diagnostic, PSTR("Upt %d<br>wfi: %d<br>cmd: %s"), uptime, connectionAttempts, command);
    diagnosticStream.setString(diagnostic);
    char diagnosticRequest[50];
    sprintf_P(diagnosticRequest, PSTR("/write/diagnostic/%s"), robotId);
    return httpClient.postFile(commandServer, diagnosticRequest, commandServerPort, &diagnosticStream, strlen(diagnostic));
}

void setup(){
#if !defined(ARDUINO_ARCH_SAMD) && !defined(ARDUINO_AVR_UNO_WIFI_REV2) && !defined(ARDUINO_SAMD_NANO_33_IOT)
    Serial.begin(115200);
    WiFi.init(Serial);
#endif
    motorController.setup();
    pinMode(inputPin,INPUT);
    pinMode(outputPin,OUTPUT);
    statusTimer = 0;
    commandTimer = millis();
    command[0] = 0;
}

void loop() {
#ifndef CAMERA_DISABLED
    if (!cameraOK) {
        cameraOK = cameraStream.begin(CAM_CS_PIN);
    }
#endif
    if (millis() - statusTimer > 20*1000) {
        if(connectionOK) {
            statusTimer = millis();
            connectionOK = reportDiagnostic() != 0x0;
        }
    }
    if (!connectionOK) {
        #ifdef ARDUINO_SAMD_MKRGSM1400
        connectionOK = (gsmAccess.begin(PINNUMBER) == GSM_READY) &&
                       (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY);
        #elif ARDUINO_SAMD_MKRNB1500
        connectionOK = (nbAccess.begin(PINNUMBER) == NB_READY) &&
                       (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY);
        #elif defined(ARDUINO_AVR_UNO_WIFI_REV2) || defined(ARDUINO_SAMD_NANO_33_IOT)
        connectionOK = WiFi.begin(ssid, password) == WL_CONNECTED;
        #else
        connectionOK = WiFi.begin(ssid, password) == WL_CONNECTED;
        #endif
        connectionAttempts++;
        if (connectionOK) {
            connectionOK = reportDiagnostic() != 0;
        }
    } else {
        if (millis() - commandTimer > commandPeriod) {
            commandTimer = millis();
            char request[50];
            sprintf_P(request, PSTR("/read/command/%s"), robotId);
            const char* httpBody = httpClient.get(commandServer, request, commandServerPort);
            connectionOK = httpBody != 0;
            if (httpBody && strlen(httpBody) > 0) {
                strcpy(command, httpBody);
                for (unsigned int i = 0; i < strlen(command); ++i) {
                    switch(command[i]) {
                        case 'a':
                        case 'w':
                        case 's':
                        case 'd':
                            motorController.execute(command[i]);
                            delay(300);
                            motorController.execute('x');//stop
                            break;
                        case 'm':
                            char ms[70];
                            ms[0] = 0;
                            myservo.attach(servoPin);
                            scandistances(inputPin, outputPin, myservo, ms);
                            myservo.write(90);
                            myservo.detach();
                            connectionOK = reportMeasurement(ms) != 0x0;
                            break;
                        case 'l':
                          autopilot(motorController);
                          break;
#ifndef CAMERA_DISABLED
                        case 'p':
                            if (cameraStream.capture()) {
                                sprintf_P(request,PSTR("/write/photo/%s"), robotId);
                                connectionOK = httpClient.postFile(commandServer, request, commandServerPort, &cameraStream, cameraStream.fifoSize()) != 0x0;
                            }
                            break;
#endif
                        default:
                            break;
                    }
                }
                motorController.execute('x');//stop
                reportDiagnostic();
            } else {
                motorController.execute('x');
            }
        }
    }
}

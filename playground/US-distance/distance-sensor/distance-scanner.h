#include <Servo.h>

float readDistance(int inputPin, int outputPin) {
  digitalWrite(outputPin,LOW);
  delayMicroseconds(2);
  digitalWrite(outputPin,HIGH);
  delayMicroseconds(10);
  digitalWrite(outputPin,LOW);
  float pingTime = pulseIn(inputPin,HIGH);
  float distance = pingTime*0.034/2;
  return distance;
}

void scandistances(int inputPin,int outputPin,Servo myservo,char*ms){
  for(int i=0;i<18;i=i+1){
    myservo.write(i*10);
    int distance= readDistance(inputPin,outputPin);
    strcat(ms,String(distance).c_str());
    strcat(ms,",");
    delay(200);
  }
}

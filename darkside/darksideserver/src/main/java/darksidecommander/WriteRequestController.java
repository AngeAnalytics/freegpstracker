package darksidecommander;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class WriteRequestController {

    @Value("${commandpath}")
    private String commandPath;

    @Value("${measurementpath}")
    private String measurementPath;

    @Value("${photopath}")
    private String photoPath;

    @Value("${diagnosticpath}")
    private String diagnosticPath;

    @GetMapping("/write/command/{robotId}/{content}")
    public String writeCommand(@PathVariable String robotId, @PathVariable  String content) throws IOException {
        RobotStatusService.commandWasReceivedFor(robotId);
        return StorageService.writeRequestToStorage(robotId, content.getBytes(), commandPath);
    }

    @GetMapping("/write/measurement/{robotId}/{content}")
    public String writeMeasurement(@PathVariable String robotId, @PathVariable  String content) throws IOException {
        return StorageService.writeRequestToStorage(robotId, content.getBytes(), measurementPath);
    }

    @PostMapping("/write/photo/{robotId}")
    public String uploadPhoto(@PathVariable String robotId, @RequestPart("file") MultipartFile file) throws IOException {
        return StorageService.writeRequestToStorage(robotId, file.getBytes(), photoPath);
    }

    @PostMapping("/write/diagnostic/{robotId}")
    public String uploadFile(@PathVariable String robotId, @RequestPart("file") MultipartFile file) throws IOException {
        return StorageService.writeRequestToStorage(robotId, file.getBytes(), diagnosticPath);
    }

}
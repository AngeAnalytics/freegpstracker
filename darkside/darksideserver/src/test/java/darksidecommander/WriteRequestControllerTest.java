package darksidecommander;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.TestPropertySource;

@TestPropertySource("classpath:test.properties") // not working
public class WriteRequestControllerTest {
    @Value("${commandpath}")
    private String commandPath;

    WriteRequestController writeRequestController = new WriteRequestController();

    ReadRequestController readRequestController = new ReadRequestController();

    @Test
    public void fileIsWritten() throws IOException {
        writeRequestController.writeCommand("1", "aaa");
        final String[] fileList = new File(commandPath + "/1").list();
        assertNotEquals(0, fileList.length);
        assertEquals("aaa", readRequestController.readCommand("1", Optional.empty()));
        assertEquals("", readRequestController.readCommand("1", Optional.empty()));
    }

    @Test
    public void fileIsWrittenNegativeLookup() throws IOException {
        writeRequestController.writeCommand("1", "aa1");
        writeRequestController.writeCommand("1", "aa2");
        writeRequestController.writeCommand("1", "aa3");
        final String[] fileList = new File(commandPath + "/1").list();
        assertNotEquals(0, fileList.length);
        assertEquals("aa1", readRequestController.readCommand("1", Optional.of(-2)));
        assertEquals("aa2", readRequestController.readCommand("1", Optional.of(-1)));
    }
}

function radarchart(id, data) {

	const height = 500;
	const width = 500;
	d3.select(id).select("svg").remove();
	var svg = d3.select(id).append("svg")
	   .attr("width", width)
	   .attr("height", height)

	var angle_slice = Math.PI / 18; //10 degrees in radians
    var r_scale = d3.scaleLinear()
        .domain([0, 1.5*d3.max(data, function(d){return d.value;})])
        .range([0, 300]);

	var radar_line = d3.lineRadial()
	    .radius(function(d) { return r_scale(d.value); })
	    .angle(function(d,i) {  return i*angle_slice; });

	var data_area = svg.selectAll('.radarWrapper')
		.data([data])
		.enter().append('g')
		.attr('class', 'radarWrapper');

	data_area.append('path')
        .attr("transform", "translate(" + width/2 + "," + height/2 + ") rotate(-90)")
		.attr('class', 'radarStroke')
		.attr('d', function(d) {
		      return radar_line(d);
		      })
		.style('stroke-width', 2)
		.style('stroke', 'black')
		.style('fill', 'none')
}

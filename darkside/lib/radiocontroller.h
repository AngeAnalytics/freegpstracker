/*
 * WiFi-based radio controller
 *
 * Set up the joystick to send via netcat (extract the channel and value positions, then send):
 *  cat  /dev/input/event20 | stdbuf -o0 xxd -c 24 -ps | stdbuf -o0 cut -c33,34,37,38,41,42 | stdbuf -o0 grep -v "^00" | stdbuf -o0 xxd -r -p | nc 192.168.4.1 3000
 */

#include "WiFiEspAT.h"

struct RadioController {
    const char* ssid;
    int8_t values[4];

    RadioController(const char* _ssid) : ssid(_ssid), tcpServer(WiFiServer(3000)), values({0, 0, 0, 0}) {};

    void init() {
        Serial.begin(115200);
        WiFi.init(Serial);
        WiFi.beginAP(ssid);
        tcpServer.begin();
    }

    int read() {
        const long int currenTime = millis();
        //reading through the AT interface is a blocking operation. Only do it every samplePeriod milliseconds.
        if (currenTime - sampleTimer > samplePeriod) {
            sampleTimer = currenTime;
            if (WiFiClient newClient = tcpServer.accept()) {
                client = newClient;
            }
            uint8_t tcpRxBuffer[296];
            const int nbBytesRead = client.read(tcpRxBuffer, 296);
            for (int i = 0; i < nbBytesRead; i += 3) {
                values[tcpRxBuffer[i+1]] = tcpRxBuffer[i+2];
            }
            return nbBytesRead;
        } else {
            return 0;
        }
    }

    private:
        WiFiServer tcpServer;
        WiFiClient client;
        long int sampleTimer = 0;
        const int samplePeriod = 500;
};

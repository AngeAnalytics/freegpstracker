#ifndef ABSTRACTTCPCLIENT_H
#define ABSTRACTTCPCLIENT_H

#include <Client.h>
#define AT_TOKEN_BUFFER 20
#define TCP_BUF_SIZE 96

#ifdef ARDUINO_ARCH_SAMD
    #define sprintf_P sprintf
#endif

#define serialTimeOut 1000
#define errorTerminator "ERROR"

/*
 * A base class for TCP clients (Esp8266 and SIM7000 at the time of writing)
 *
 */

class AbstractTcpClient : public Client {
  public:
    AbstractTcpClient(Stream* ss);
    virtual bool begin(const char* ssid, const char* pass=0x0);
    virtual int connect(const char* host, uint16_t port) = 0;
    int available(void);
    int read(void);
    int read(uint8_t *buf, size_t size) { return 0; }; //not implemented yet
    int peek(void);
    size_t write(uint8_t) { return 0; }; //not implemented
    size_t write(const uint8_t* buffer, size_t bufLength);
    void flush(void);
    int connect(IPAddress ip, uint16_t port) { return -1; }; //not implemtented
    void stop(void) {};  //not implemented
    operator bool(void) { return false; }; //not implemented
    uint8_t connected(void);

    const char* response(void) {return lastResponse;}

  protected:
    virtual bool extractTcpResponse(void);
    const char* SSID;
    const char* PASS;
    Stream* ss;
    virtual bool checkSerialResponseFor(const char* terminatorOK, const char* terminatorError, unsigned long maxWaitingTime);
    char lastResponse[AT_TOKEN_BUFFER];
    bool connectionOk = false;
    int position = 0;
    bool tcpResponseReceived = false;
    int remainingBytes = 0;
};

AbstractTcpClient::AbstractTcpClient(Stream* _ss) {
  ss = _ss;
  ss->setTimeout(serialTimeOut);
}

bool AbstractTcpClient::checkSerialResponseFor(const char* terminatorOK, const char* terminatorError, unsigned long maxWaitingTime){
    unsigned long start = millis();
    int strPos = 0;
    for(int i = 0; i < AT_TOKEN_BUFFER; ++i) {
        lastResponse[i] = 0;
    }
    tcpResponseReceived = false;
    while(millis() - start < maxWaitingTime) {
        while(!ss->available()) {
            if (millis() - start > maxWaitingTime) {
                return false;
            }
        }
        if (ss->available() && strPos < AT_TOKEN_BUFFER) {
            lastResponse[strPos] = ss->read();
            strPos++;
            start = millis();
        }
        if (strstr(lastResponse, terminatorOK)) {
            return true;
        }
        if (strstr(lastResponse, terminatorError)) {
            return false;
        }
        if (strPos == AT_TOKEN_BUFFER ) {
            strPos = AT_TOKEN_BUFFER / 2;
            memcpy(lastResponse, lastResponse + AT_TOKEN_BUFFER / 2, AT_TOKEN_BUFFER / 2);
            memset(lastResponse + AT_TOKEN_BUFFER / 2, 0, AT_TOKEN_BUFFER / 2);
        }
    }
    return false;
}

size_t AbstractTcpClient::write(const uint8_t* buffer, size_t packetLength) {
    ss->print(F("AT+CIPSEND="));
    ss->println(packetLength);
    if(!checkSerialResponseFor(">", errorTerminator, serialTimeOut)) return false;
    for(unsigned int i = 0; i < packetLength; ++i) {
        ss->write(buffer[i]);
    }
    return checkSerialResponseFor("SEND OK\r\n", errorTerminator, serialTimeOut);
}

void AbstractTcpClient::flush() {
    while (ss->available()) {
        delay(1);
        ss->read();
    }
}

bool AbstractTcpClient::extractTcpResponse(void) {
    position = 0;
    if(checkSerialResponseFor("+IPD,", errorTerminator, serialTimeOut)
    && checkSerialResponseFor(":", errorTerminator, serialTimeOut)) {
        char* end;
        remainingBytes = strtol(lastResponse, &end, 10);
    }
    return remainingBytes > 0;
}

uint8_t AbstractTcpClient::connected(void) {
    return connectionOk;
}

int AbstractTcpClient::read(void) {
    if(available() > 0) {
        char charFromSerial = ss->read();
        remainingBytes--;
        return charFromSerial;
    } else {
        return 0;
    }
}

int AbstractTcpClient::peek(void) {
    return ss->peek();
}

int AbstractTcpClient::available(void) {
    if (!tcpResponseReceived) {
        tcpResponseReceived = extractTcpResponse();
    }
    if (remainingBytes == 0) {
        return 0;
    } else {
        long int timer = millis();
        while(!ss->available() && (millis() - timer < serialTimeOut)) {
            //busy wait
        };
        if (ss->available()) {
            return remainingBytes;
        } else {
            return 0;
        }
    }
}

#endif

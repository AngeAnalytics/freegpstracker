/*
 * Open serial monitor to see if test succeeded or failed.
 */

#include "ArduCamStream.h"
ArduCamStream cameraStream;
bool cameraInitiationOk = false;
bool cameraCaptureOk = false;

#ifdef ARDUINO_ARCH_SAMD
const int CAM_CS_PIN = 7;
#else
const int CAM_CS_PIN = 10;
#endif

void setup() {
    Serial.begin(115200);
    while (!Serial) {
        //wait for Serial
    }
    cameraInitiationOk = cameraStream.begin(CAM_CS_PIN);
    Serial.println(cameraInitiationOk? "Camera initiation succeeded" : "Camera initiation failed");
    if (cameraInitiationOk) {
        cameraCaptureOk = cameraStream.capture();
    }
    Serial.println(cameraCaptureOk? "Capture succeeded" : "Capture failed");
}

void loop() {
    //do nothing
}

/**
 * A test of the combined GPS and GSM sending capabilities of
 * the SIM7000E
 *
 * This test requires that the module has both GSM and GPS connections up and running.
 *
 * Central AT commands:
 *
 * AT+CGNSPWR?
 * AT+CGNSPWR=1
 * AT+CGNSINF
 */

#include "HttpStreamClient.h"
#include "wificonfig.h"
#include "Sim7000Client.h"
#include "SoftwareSerial.h"

SoftwareSerial* ss = new SoftwareSerial(8,9);
Sim7000Client sim7000Client(ss);
HttpStreamClient httpClient(&sim7000Client);

bool testSucceeded = false;
const char* testResponse;

void setup() {
    Serial.begin(9600);
    Serial.println("Starting");
    ss->begin(9600);
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    const bool connectionOk = sim7000Client.begin(PINNUMBER);
    LatLonTime latLonTime = sim7000Client.gpsLatLonTime();
    if (connectionOk && latLonTime.time[0] != 0) {
        char measurementRequest[90];
        char measurementContent[50];
        sprintf_P(measurementContent, PSTR("%ld,%ld,%s"), latLonTime.lat, latLonTime.lon, latLonTime.time);
        sprintf_P(measurementRequest, PSTR("/write/measurement/%s/%s"), robotId, measurementContent);
        testResponse = httpClient.get(commandServer, measurementRequest, commandServerPort);
        testSucceeded = testResponse != 0x0;
        Serial.println(measurementRequest);
        Serial.println(testResponse);
    }
}

void loop() {
    const int halfPeriod = testSucceeded? 1000 : 200;
    if ((millis() / halfPeriod) % 2 == 0) {
        digitalWrite(LED_BUILTIN, HIGH);
    } else {
        digitalWrite(LED_BUILTIN, LOW);
    }
}

% A simulation of the dynamics of a quadcopter with the purpose to investigate
% how to properly choose the P and D coefficients in a PID loop.
% The simulations shows that both P and D are necessary for the drone to be
% stable.

motorPowers = zeros(4,6);
motorSpeeds = zeros(4,6);
angularAccelerations = zeros(3,6);
angularSpeeds = zeros(3,6);
orientations = [0 0 0]' * ones(1,6);
angularSpeed = zeros(3,1);
orientation = [0 0 0]'; %zeros(3,1)
%Kp: proportional term coefficient, Kd: derivative term coefficient Ka: delayed acceleration term (not PID)
%uncomment the appropriate scenario
%unstable, too large Kp
%Kp = 0.02; Kd = .18; Ka = 0; noiseLevel = 0.05;
%stable
%Kp = 0.01; Kd = .18; Ka = 0; noiseLevel = 0.05;
%unstable, too large Kd
Kp = 0.01; Kd = .30; Ka = 0; noiseLevel = 0.05;
%further stabilized with estimated (delayed) acceleration
%Kp = 0.01; Kd = .18; Ka = 0.2; noiseLevel = 0.05;

%how the error should translate into motor powers:
errorToAction = [ 1,  1, -1
                  1, -1,  1
                 -1, -1, -1
                 -1,  1,  1 ];

%response of the drone to (motor) input
responseMatrix = errorToAction * inv(errorToAction' * errorToAction);

for iteration = 1:600,
    motorSpeed = motorPowers(:, end-5);
    [angularAcceleration, angularSpeed, orientation] = dronedynamics(motorSpeed, angularSpeed, orientation, noiseLevel, responseMatrix);
    %PID correction: Note: in reality angularAcceleration would need to be calculated from the motorSpeed (delayed)
    motorPower = -errorToAction  * (Kp * orientation + Kd * angularSpeed + Ka * angularAcceleration);
    
    motorSpeeds(:,end+1) = motorSpeed;
    angularAccelerations(:,end+1) = angularAcceleration;
    angularSpeeds(:,end+1) = angularSpeed;
    orientations(:,end+1) = orientation;
    motorPowers(:,end+1) = motorPower;
endfor

hold off; plot(Kp*orientations(2,:), '.-');
hold on; plot(Kd*angularSpeeds(2,:), '.-');
title('Roll and roll speed as a function of time');
legend('Roll', 'Roll speed');

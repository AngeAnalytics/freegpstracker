/*
 * An investigation showing that the gyro is imprecise making the integration of
 * angular speed into an angle error-prone. Typicall error: 0.1 - 0.5 degrees / second
 * 
 */

#include <Arduino_LSM6DS3.h>

float pitchp, rollp, yawp;
float rollp0 = 0;
float roll = 0;
void setup() {
    Serial.begin(115200);
    if (!IMU.begin()) {
        Serial.println("Failed to initialize IMU!");
    }
    int nbSamples = 0;
    for (int i = 0; i < 1000; ++i) {
        IMU.readGyroscope(pitchp, rollp, yawp); //will read a lower value than when this is called later on, breaking the calibration
        rollp0  += rollp;
        nbSamples++;
        delay(1);
    }
    rollp0 /= nbSamples;
}

void loop() {
    if (IMU.gyroscopeAvailable()) {
        IMU.readGyroscope(pitchp, rollp, yawp);
        roll += rollp-rollp0;
        Serial.print(roll);
        Serial.print('\t');
        Serial.print(rollp);
        Serial.print('\t');
        Serial.print(rollp0);
        Serial.println();
    }
}

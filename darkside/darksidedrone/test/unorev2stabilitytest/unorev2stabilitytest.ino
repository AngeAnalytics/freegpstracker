/*
 * Test the stability when sending over UDP
 * On the server, run
 *    nc -l -u -p 9999 | nc -u 192.168.43.79 3000
 * and watch the output in the serial console.
 *
 * In case it stops (a lost packet) send a "bootstrap packet" with
 *    nc -u 192.168.43.79 3000
 *
 */

#include <WiFiNINA.h>
#include <WiFiUdp.h>
const char ssid[] = "Jolly 1";
const char password[] = "aaaaaaaa";
const IPAddress serverIp(192,168,43,112);
const int serverPortNumber = 9999;
WiFiUDP udp;
unsigned long iterationCounter = 0;;

void sendPingStringOverUDP(const char* pingString) {
    udp.beginPacket(serverIp, serverPortNumber);
    udp.write(pingString, strlen(pingString));
    udp.endPacket();
}

void setup() {
    Serial.begin(115200);
    WiFi.begin(ssid, password);
    udp.begin(3000);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    sendPingStringOverUDP("ping");
}

void loop() {
    iterationCounter++;
    digitalWrite(LED_BUILTIN, (iterationCounter/1000) % 2);
    const int connectionStatus = WiFi.status();
    if(connectionStatus != WL_CONNECTED) {
        Serial.println("Connection lost. Reconnecting.");
        if(WiFi.begin(ssid, password) == WL_CONNECTED) {
            udp.begin(3000);
            Serial.print("IP address: ");
            Serial.println(WiFi.localIP());
            sendPingStringOverUDP("ping");
        } else {
            Serial.println("Connection failed");
        }
    }
    if (udp.parsePacket() > 0) {
        uint8_t udpReceiveBuffer[48];
        udp.read(udpReceiveBuffer, 48);
        const char pingString[200];
        sprintf(pingString, "count: %lu and a large payload of many bytes to test the robustness when sending larger packets.\n", iterationCounter);
        sendPingStringOverUDP(pingString);
        Serial.print(pingString);
    } else {
        if ((iterationCounter % 1000) == 0) {
            const char debugString[200];
            const bool isConnected =    connectionStatus == WL_CONNECTED;
            sprintf(debugString, "count: %lu, no packet received, modulo check:%lu, wifi: %d, wifi connected? %d", iterationCounter, (iterationCounter % 1000), connectionStatus, isConnected);
            Serial.println(debugString);
        }
    }
}

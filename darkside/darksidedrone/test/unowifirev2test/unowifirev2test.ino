/*
 * A drone using an Arduino Uno Wifi Rev. 2 using a PID controller (without the I for now)
 * To debug over wifi, connect to drone via its AP:
 *   nc 192.168.4.1 3000
 *
 * Set up the joystick to send via UDP:
 *  cat  /dev/input/event20 | nc 192.168.4.1 3000
 *
 * Motor configuration
 * forward
 *  0 1
 *  3 2
 * back
 */

#include <Arduino_LSM6DS3.h>
#include <Servo.h>
#include <WiFiNINA.h>
const char ssid[] = "efdrobot";
const float Kp = 0.5; //coefficient for the proportional term of the PID
const float Kd = 20; //coefficient for the derivative term of the PID
const float sampleTime = 1.0/104.0;//fixed for this board's IMU
const float alpha = 0.98; //weight of gyro measurement relative to accelerometer measurement (complementary filter)
const int frequencyDivider = 2; //reduce the sampling and controlling rate to see how low one can get (2 means ~50 Hz)
int sampleCounter = 0; //used to limit the flight time (for safety reasons)
bool isArmed = false; //needs to be true for the drone to fly, set over UDP
float orientation[] = {0, 0, 0}; //pitch, roll, yaw
float gyroDelta[3]; //increment in orientation since last sample (as measured by the gyro)
float gyrop[3]; //rotational velocities in degrees per second, {pitch', roll', yaw'}
float acc[3]; //accelerations in g (as measured by the accelerometer)
Servo motors[4]; //ESCs are controlled using PWM just like servos
const int motorPins[] = {5,6,9,10};
int8_t ctrlValues[] = {0, 0, 0, 0}; //target pitch, roll, yaw, power
float currentMotorPulseWidth;
long int iterationCounter = 0;
WiFiServer tcpServer(3000);
int currentByte = 0;
int prevMotorPowers[][4] = {{0,0,0,0}, {0,0,0,0}, {0,0,0,0}, {0,0,0,0}};

void setup() {
    WiFi.beginAP(ssid);
    tcpServer.begin();
    IMU.begin();
    for (int i = 0; i < 4; ++i) {
        motors[i].attach(motorPins[i]);
        motors[i].writeMicroseconds(0);
    }
}

void loop() {
    iterationCounter++;
    WiFiClient client = tcpServer.available();
    if (client.available()) {
        uint8_t tcpRxBuffer[8]; //The HID packets 24 bytes long and somehow need to be read 8 bytes at the time (WiFiNina?)
        const int nbBytesRead = client.read(tcpRxBuffer, 8);
        if (currentByte == 2 && tcpRxBuffer[0]) {
           //In the USB HID packet position 18 is channel, 20 is value, enabled is 16 (== 3),
            ctrlValues[tcpRxBuffer[2]] = (int8_t)tcpRxBuffer[4];
        }
        currentByte ++;
        currentByte %= 3;
    }
    if (!isArmed) {
        digitalWrite(LED_BUILTIN, (iterationCounter / 200) % 2 == 0);
        for (int i = 0; i < 4; ++i) {
            motors[i].writeMicroseconds(0);
        }
        if (ctrlValues[0] < -120 && ctrlValues[1] > 120 && ctrlValues[2] < -120 && ctrlValues[3] < -120) {
            isArmed = true;
        }
    }
    if (isArmed && IMU.accelerationAvailable() && IMU.gyroscopeAvailable()) {
        sampleCounter ++;
        IMU.readAcceleration(acc[0], acc[1], acc[2]);
        IMU.readGyroscope(gyrop[0], gyrop[1], gyrop[2]);
        if (sampleCounter  % frequencyDivider == 0)  {
            float nextOrientation[3];
            using F = float();
            F* updateFunctions[3] = {
                []() {return alpha * (orientation[0] + gyroDelta[0]) + (1 - alpha) *  atan2(acc[1], acc[2] * RAD_TO_DEG);},
                []() {return alpha * (orientation[1] + gyroDelta[1]) + (1 - alpha) * -atan2(acc[0], acc[2] * RAD_TO_DEG);},
                []() {return          0*(orientation[2] + gyroDelta[2]  - 0.0 * ctrlValues[2]);} //TODO: remove or use complementary filter
            };

            float errors[3]; //pitch, roll, yaw
            for (int j = 0; j < 3; ++j) {
                gyroDelta[j] = gyrop[j] * sampleTime * frequencyDivider;
                nextOrientation[j] = updateFunctions[j]();
                orientation[j] = nextOrientation[j];
                errors[j] = orientation[j] - map(ctrlValues[j], -128, 127, -200, 200);
            }

            const float errorToAction[][3] = { { 1,  1, -1},
                                               { 1, -1,  1},
                                               {-1, -1, -1},
                                               {-1,  1,  1} };
            int motorPowers[4];
            for (int i = 0; i < 4; ++i) {
                int motorPower =  map(ctrlValues[3], -128, 127, 0, 600)+1000;
                for (int j = 0; j < 3; ++j) {
                    motorPower += errorToAction[i][j] * (Kp * errors[j] + Kd * gyroDelta[j]);
                }
                motorPower = 0.7* motorPower + 0.3 * prevMotorPowers[0][i];
                motors[i].writeMicroseconds(motorPower);
                motorPowers[i] = motorPower;
                for (int k = 0; k < 3; ++k) {
                    prevMotorPowers[k][i] = prevMotorPowers[k+1][i];
                }
                prevMotorPowers[3][i] = motorPower;
            }

            char debugString[70];
            sprintf(debugString, "%5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d\n", sampleCounter,
                    (int)(orientation[0]*100), (int)(orientation[1]*100), (int)(orientation[2]*100),
                    (int)(gyrop[0]*100), (int)(gyrop[1]*100), (int)(gyrop[2]*100),
                    motorPowers[0], motorPowers[1], motorPowers[2], motorPowers[3],
                    ctrlValues[0], ctrlValues[1], ctrlValues[2], ctrlValues[3]);
            tcpServer.write(debugString, strlen(debugString));
        }
    }
}

/*
 * First take at a drone using an Arduino Uno Wifi Rev. 2
 * To debug over wifi, run (on a receiving server on the same subnet):
 *   nc -l -u -p 9999
 */

#include <Arduino_LSM6DS3.h>
#include <Servo.h>
#include <WiFiNINA.h>
#include <WiFiUdp.h>
#define DEBUG_OVER_WIFI
#define sampleTime  1/104
#define alpha 0.98 //weight of gyro measurement relative to accelerometer measurement (complementary filter)
#define frequencyDivider 2 //reduce the sampling and controlling rate

float gyrop[] = {0, 0, 0}; ////rotational velocities in degrees per second, {pitch', roll', yaw'}
float gyroAngleXDelta, pitch, prevPitch = 0.0;
float gyroAngleYDelta, roll, prevRoll = 0.0;
float gyroAngleZDelta, yaw, prevYaw = 0.0;
float accX, accY, accZ; //accelerations in g
float currentAngleXGyro, currentAngleXAcc;
float currentAngleYGyro, currentAngleYAcc;
float currentAngleZGyro;
int sampleCounter = 0;

// Motor configuration
//forward
// 0 1
// 3 2
//back

Servo motors[4];
int motorPins[] = {5,6,9,10};

int sustainedFlightPulseWidth = 1360; //1500 //may vary depending on ESC
float Kp = 0.8;
float Kd = 30;
float trimPitch = 0.0; // + means trim forward
float trimRoll = 0.0; //- means trim roll left
float trimYawGyro = 0.1;

const char ssid[] = "WiFimodem-A7B6";
const char password[] = "qjy4egn4kd";
const IPAddress serverIp(192,168,0,37);
const int serverPortNumber = 9999;
WiFiUDP udp;

void setup() {
    Serial.begin(115200);
#ifdef DEBUG_OVER_WIFI
     if (WiFi.begin(ssid, password) != WL_CONNECTED) {
        Serial.println("Wifi connection failed");
    } else {
        udp.begin(3000);
    }
#endif

    IMU.begin();
    for (int i = 0; i < 4; ++i) {
        motors[i].attach(motorPins[i]);
        motors[i].writeMicroseconds(0);
    }
    delay(3000); //allow ESPs to initialize
}

void loop() {
    if (sampleCounter > 250) { //note: millis() won't work with WiFi
        Serial.println("Max time reached");
        for (int i = 0; i < 4; ++i) {
            motors[i].writeMicroseconds(0);
        }
        while(true) {}
    }

    if (IMU.accelerationAvailable() && IMU.gyroscopeAvailable()) {
        sampleCounter ++;
        IMU.readAcceleration(accX, accY, accZ);
        IMU.readGyroscope(gyrop[0], gyrop[1], gyrop[2]);
        if (sampleCounter  % frequencyDivider == 0)  {
            gyroAngleXDelta = gyrop[0] * sampleTime * frequencyDivider;
            currentAngleXGyro = (prevPitch + gyroAngleXDelta);
            currentAngleXAcc = atan2(accY, accZ) * RAD_TO_DEG;
            pitch = alpha * currentAngleXGyro + (1 - alpha) * currentAngleXAcc;
            prevPitch = pitch;

            gyroAngleYDelta = gyrop[1] * sampleTime * frequencyDivider;
            currentAngleYGyro = (prevRoll + gyroAngleYDelta);
            currentAngleYAcc = -atan2(accX, accZ) * RAD_TO_DEG;
            roll = alpha * currentAngleYGyro + (1 - alpha) * currentAngleYAcc;
            prevRoll = roll;

            gyroAngleZDelta = gyrop[2] * sampleTime * frequencyDivider;
            yaw = (prevYaw + gyroAngleZDelta - trimYawGyro);
            prevYaw = yaw;
            const float errorToAction[][3] = { { 1,  1, -0.1},
                                               { 1, -1,  0.1},
                                               {-1, -1, -0.1},
                                               {-1,  1,  0.1}};
            float errors[3];
            errors[0] = pitch - trimPitch;
            errors[1] = roll - trimRoll;
            errors[2] = yaw;
            int motorPowers[4];
            for (int i = 0; i < 4; ++i) {
                int motorPower =  sustainedFlightPulseWidth;
                for (int j = 0; j < 3; ++j) {
                    motorPower += errorToAction[i][j] * (Kp * errors[j] + Kd * gyrop[j] * sampleTime * frequencyDivider);
                }
                motors[i].writeMicroseconds(motorPower);
                motorPowers[i] = motorPower;
            }

            char debugString[70];
            sprintf(debugString, "%5d %5d %5d %5d %5d %5d %5d %5d %5d %5d %5d\n", sampleCounter, (int)(pitch*100), (int)(roll*100), (int)(yaw*100), (int)(gyrop[0]*100), (int)(gyrop[1]*100), (int)(gyrop[2]*100), motorPowers[0], motorPowers[1], motorPowers[2], motorPowers[3]);
#ifdef DEBUG_OVER_WIFI
            udp.beginPacket(serverIp, serverPortNumber);
            udp.write(debugString, strlen(debugString));
            udp.endPacket();
#else
            Serial.println(debugString);
#endif
        }
    }
}
